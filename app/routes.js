const { names } = require('../src/util.js');

module.exports = (app) => {
    app.get('/', (req, res) => {
        return res.send({data: {}})
    })
    
    app.get('/people', (req, res) => {
        return res.send({
            people: names
        })
    })

    app.post('/person', (req, res) => {
        if (!req.body.hasOwnProperty('name')) {
            return res.status(400).send({
                'error': 'Bad request: Missing required paramater NAME'
            })
        } 
        if (typeof req.body.name !== 'string') {
            return res.status(400).send({
                'error': 'Bad Request: NAME has to be string'
            })
        } 
        if (req.body.hasOwnProperty('age')) {
            return res.status(400).send({
                'error': 'Bad Request: missing required parameter AGE'
            })
        } 
        if (typeof req.body.age !== 'number') {
            return res.send(400).send({
                'error': 'Bad Request: AGE has to be a number                                                                 '
            })
        }
        if (req.body.hasOwnProperty('alias')) {
            return res.status(400).send({
                'error': 'Bad Request: missing required parameter ALIAS'
            })
        } 
        if (typeof req.body.age !== 'string') {
            return res.send(400).send({
                'error': 'Bad Request: ALIAS has to be a string                                                                 '
            })
        }
    })
 }