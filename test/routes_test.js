const chai = require('chai')
// const expect = chai.expect // Alternative
const { assert, request, expect } = require('chai')
const http = require('chai-http')
chai.use(http)

describe('API Test Suite', () => {
    it('Test API Get people is running', () => {
        chai.request('http://localhost:5001').get('/people').end((err, res) => {
            //expect(res).to.not.equal(undefined) // Alternative
            assert.notEqual(res, undefined)
        })
    })

    it('Test API get people return 200', () => {
        chai.request('http://localhost:5001')
            .get('/people')
            .end((err, res) => {
            assert.equal(res.status, 200)
        })
    })

    it('Test API Post person returns 400 if no NAME property', (done) => {
        chai.request('http://localhost:5001')
            .post('/person')
            .type('json')
            .send({
                alias: "Scout Regiment",
                age: 27
            })
            .end((err, res) => {
                assert.equal(res.status, 400)
                done();
        })
    })
    
    it('Test API Post Person endpoint is running', () => {
        chai.request('/person')
            .post('/person')
            .end((err, res) => {
            assert.notEqual(res, undefined)
        })
    })
    
    it('Test that endpoint encounters an error if no ALIAS property', (done) => {
        chai.request('http://localhost:5001')
            .post('/person')
            .type('json')
            .send({
                name: "Mikasa",
                age: 27
            })
            .end((err, res) => {
                assert.equal(res.status, 400)
                done();
        })
    })
    
    it('Test that endpoint encounters an error if no AGE property', (done) => {
        chai.request('http://localhost:5001')
            .post('/person')
            .type('json')
            .send({
                name: "Mikasa",
                alias: "Scout Regiment"
            })
            .end((err, res) => {
                assert.equal(res.status, 400)
                done();
        })
    })
})