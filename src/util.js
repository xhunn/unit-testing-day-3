const factorial = (n) => {
    if (n === 0 || n === 1) {
        return 1
    } else {
        return n * factorial(n-1)
    }
}

const divisibleBySeven = (number) => {
    if (number === 0){
        return false
    } else if (number % 7 === 0) {
        return true
    } else {
        return false
    }
}

const divisibleByFive = (number) => {
    if (number === 0){
        return false
    } else if (number % 5 === 0) {
        return true
    } else {
        return false
    } 
}

const names = {
    "Eren": {
        "name": "Eren Yeager",
        "age": 28,
        "alias": "Scout Regiment"
    },
    "Levi": {
        "name": "Levi Ackerman",
        "age": 38,
        "alias": "Scout Regiment"
    }
}

module.exports = {
    names: names,
    factorial: factorial,
    divisibleByFive: divisibleByFive,
    divisibleBySeven: divisibleBySeven
}